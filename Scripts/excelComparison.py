# ~~~
# The MIT License (MIT)

# Copyright (c) 2020 - 2021 Christian Pfeuffer.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ~~~

# ~~~
# The following code is a modified version for personal purposes from Matthew Kudija.
# The Original can be found under the following link: 
# https://github.com/mkudija/blog/blob/master/content/downloads/code/excel-diff/excel-diff.py
# ~~~ 


import pandas as pd
import pathlib

def excel_diff(file_OLD,file_NEW,outputFile):
    df_OLD = pd.read_excel(file_OLD).fillna(0)
    df_NEW = pd.read_excel(file_NEW).fillna(0)

    dfDiff = df_OLD.copy()
    for row in range(dfDiff.shape[0]):
        for col in range(dfDiff.shape[1]):
            value_OLD = df_OLD.iloc[row,col]
            try:
                value_NEW = df_NEW.iloc[row,col]
                if value_OLD==value_NEW:
                    dfDiff.iloc[row,col] = df_NEW.iloc[row,col]
                else:
                    dfDiff.iloc[row,col] = ('{}-->{}').format(value_OLD,value_NEW)
            except:
                dfDiff.iloc[row,col] = ('{}-->{}').format(value_OLD, 'NaN')

    writer = pd.ExcelWriter(outputFile, engine='xlsxwriter')

    dfDiff.to_excel(writer, sheet_name='DIFF', index=False)
    df_NEW.to_excel(writer, sheet_name=file_NEW.stem, index=False)
    df_OLD.to_excel(writer, sheet_name=file_OLD.stem, index=False)

    workbook  = writer.book
    worksheet = writer.sheets['DIFF']
    worksheet.hide_gridlines(2)

    # define formats

    red_fmt = workbook.add_format({'font_color': '#e30909'})
    highlight_fmt = workbook.add_format({'font_color': '#e30909', 'bg_color':'#ddff00'})

    ## highlight changed cells
    worksheet.conditional_format('D1:I3729', {'type': 'text',
                                            'criteria': 'containing',
                                            'value':'→',
                                            'format': highlight_fmt})
    ## highlight unchanged cells
    worksheet.conditional_format('D1:I3729', {'type': 'text',
                                            'criteria': 'not containing',
                                            'value':'→',
                                            'format': red_fmt})
    # save
    writer.save()

    print('done')

def main():
    df_OLD = pathlib.Path('/home/hamdi/Desktop/workspace/git_py/master/Tabellen/tabelle1.xlsx')
    df_NEW = pathlib.Path('/home/hamdi/Desktop/workspace/git_py/master/Tabellen/tabelle2.xlsx')
    df_output = pathlib.Path('/home/hamdi/Desktop/workspace/git_py/master/Tabellen/tabelle3.xlsx')

    excel_diff(df_OLD, df_NEW, df_output)

if __name__ == '__main__':
    main()

